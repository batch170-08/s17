let student = [];

function addStudent(name) {
	student.push(name);
	console.log(`${name} is added to the student's list`);
};

addStudent("John");
addStudent("Jane");
addStudent("Joe");

function countStudents() {
	console.log(`There are a total of ${student.length} students enrolled`)
}

countStudents();

function printStudents() {
	student.sort();

	student.forEach(
		function(element){
			console.log(element)
		}
	)
};

printStudents();

function findStudent(match){
	let filterStudent = student.filter(
		function(element){
		return element.toLowerCase().includes(match.toLowerCase())
		}
	)

	if(filterStudent.length === 1){
		console.log(`${filterStudent} is an Enrollee`);

	} else if(filterStudent.length > 1){
		console.log(`${filterStudent} are Enrollees`);

	} else {
		console.log(`No student found with name ${filterStudent}`);
	}
};

findStudent("J");


function addSection(section) {
	let studentSection = student.map (
		function(students) {
			console.log(`${students} - Section ${section}`)
		}
	)
};

addSection("A");

// function removeStudent()













